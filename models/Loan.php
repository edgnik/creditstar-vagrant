<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Loans".
 *
 * @property integer $loanId
 * @property integer $userId
 * @property string $amount
 * @property string $interest
 * @property integer $duration
 * @property string $dateApplied
 * @property string $dateLoanEnds
 * @property integer $campaign
 * @property boolean $status
 * @property User $user
 */
class Loan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Loans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'amount', 'interest', 'duration', 'dateApplied', 'dateLoanEnds', 'campaign', 'status'], 'required'],
            [['userId', 'duration', 'campaign', 'status'], 'integer'],
            [['amount', 'interest'], 'number'],
            [['dateApplied', 'dateLoanEnds'], 'safe'],
            [['userId'], 'exist', 'targetClass' => 'app\models\User', 'targetAttribute' => 'userId'],
            [['dateApplied', 'dateLoanEnds'], 'validateDate'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'loanId' => 'Loan ID',
            'userId' => 'User ID',
            'amount' => 'Amount',
            'interest' => 'Interest',
            'duration' => 'Duration',
            'dateApplied' => 'Date Applied',
            'dateLoanEnds' => 'Date Loan Ends',
            'campaign' => 'Campaign',
            'status' => 'Status',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['userId' => 'userId']);
    }

    /**
     * @inheritdoc
     */
    public function validateDate($date, $params)
    {
        $format = 'Y-m-d';
        $d = \DateTime::createFromFormat($format, $this->$date);

        if (!($d AND $d->format($format) == $this->$date))
        {
            $this->addError($date, 'The date is not valid.');
        }
    }
}
