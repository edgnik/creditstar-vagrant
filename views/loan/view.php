<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Loan */

$this->title = 'Loan: '.$model->loanId;
$this->params['breadcrumbs'][] = ['label' => 'Loans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->loanId;
?>
<div class="view-container loan-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->loanId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->loanId], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'loanId',
            //'userId',
            [
                'label' => 'User ID',
                'format' => 'raw',
                'value' => Html::a($model->userId, ['user/view', 'id' => $model->userId], ['target'=>'_blank']),
            ],
            'amount',
            'interest',
            'duration',
            'dateApplied',
            'dateLoanEnds',
            'campaign',
            'status',
        ],
    ]) ?>

</div>
