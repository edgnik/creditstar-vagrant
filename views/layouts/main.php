<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">

        <div class='top-bar'>
            <section class="left">
                <ul class="customer-service">
                    <li><b>Klienditeenindus</b></li>
                    <li><img src="<?= Yii::$app->request->getBaseUrl() ?>/img/ico-customerservice.png"/><b> 1715</b></li>
                    <li><img src="<?= Yii::$app->request->getBaseUrl() ?>/img/ico-openingtimes.png"/><b> E-P 9:00-21:00</b></li>
                </ul>
            </section>
            <section class="right">
                <ul class="authentication">
                    <li><button type="button" class="btn btn-primary">LOG IN</button></li>
                    <li><button type="button" class="btn btn-primary">REGISTER</button></li>
                </ul>
            </section>
        </div>

        <div class='logo-bar'>
            <?php
                NavBar::begin([
                    'brandLabel' => '<img class="logo" src="' . Yii::$app->request->getBaseUrl() . '/img/logo-creditstar.png">',
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-white navbar-fixed-top',
                    ],
                ]);
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-center'],
                    'items' => [
                        ['label' => 'Home', 'url' => ['/site/index']],
                        ['label' => 'Users', 'url' => ['/user/index']],
                        ['label' => 'Loans', 'url' => ['/loan/index']],
                        ['label' => 'About', 'url' => ['#']],
                        ['label' => 'Contact', 'url' => ['#']],
                    ],
                ]);
                NavBar::end();
            ?>
        </div>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="text-center">&copy; Creditstar <?= date('Y') ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
