<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'User: '.$model->userId;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->userId;
?>
<div class="view-container user-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr>

        <p>
            <?= Html::a('Update', ['update', 'id' => $model->userId], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Create Loan', ['loan/create', 'userId' => $model->userId], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->userId], [
                'class' => 'btn btn-danger pull-right',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'userId',
                'firstName:ntext',
                'lastName:ntext',
                [
                    'label' => 'Age',
                    'value' => $age,
                ],
                'email:ntext',
                'personalCode',
                'phone',
                'active:boolean',
                'isDead:boolean',
    //            'lang:ntext',
                [
                    'label' => 'Languange',
                    'value' => $lang,
                ],
            ],
        ]) ?>

    </div>

</div>
