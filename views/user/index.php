<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="view-container user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}\n<div align='center'>{pager}</div>",
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'userId',
                'firstName:ntext',
                'lastName:ntext',
                'email:ntext',
                //'personalCode',
                 'phone',
                // 'active:boolean',
                // 'isDead:boolean',
                // 'lang:ntext',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

</div>
