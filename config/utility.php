<?php

use DateTime;

$languages = [
    "eng" => "English",
    "est" => "Estonian",
    "rus" => "Russian",
];
	
function getAge($personalCode)
{		
  $currentDate = new DateTime();
		
	$parsedDate = substr($personalCode, 1, 6); // GYYMMDDSSSC
	$parsedDate = DateTime::createFromFormat('ymd', $parsedDate);
	$parsedDate = new DateTime($parsedDate->format('Y-m-d'));
		
	$diff=date_diff($parsedDate, $currentDate);
		
	return $diff->y;
}
	
function getLanguage($key) {

  global $languages;

	return $languages[$key];
}

?>
